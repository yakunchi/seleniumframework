package julia.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OurPassionPage {
    public WebDriver driver;

    public OurPassionPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "[id*='914914']")
    private WebElement ourPassionTitle;

    @FindBy(css = "[id*='914903']")
    private WebElement theExpertsTitle;

    public WebElement getOurPassionTitle() {
        return ourPassionTitle;
    }

    public WebElement getTheExpertsTitle() {
        return theExpertsTitle;
    }

}
