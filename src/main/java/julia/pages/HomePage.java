package julia.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    public WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[@data-title='Welcome']")
    private WebElement welcomeLeftSideMenuLink;

    @FindBy(xpath = "//a[@data-title='Our Passion']")
    private WebElement ourPassionLeftSideMenuLink;

    @FindBy(xpath = "//a[@data-title='Menu']")
    private WebElement menuLeftSideMenuLink;

    @FindBy(xpath = "//a[contains(@data-title,'Talk Tea')]")
    private WebElement letsTalkLeftSideMenuLink;

    @FindBy(xpath = "//a[@data-title='Check Out']")
    private WebElement checkOutLeftSideMenuLink;

    @FindBy(xpath = "(//*[@class= 'editor_sidebarmore'])[2]")
    private WebElement organicTeaLinkSideBar;

    @FindBy(xpath = "(//*[@class= 'editor_sidebarmore'])[1]")
    private WebElement teaOfMonthClubLinkSideBar;

    @FindBy(xpath = "(//span[@class= 'button-content wsb-button-content'])[2]")
    private WebElement herbalTeaCollectionButton;

    @FindBy(xpath = "(//span[@class= 'button-content wsb-button-content'])[3]")
    private WebElement looseTeaCollectionButton;

    @FindBy(xpath = "(//span[@class= 'button-content wsb-button-content'])[1]")
    private WebElement falvouredTeaCollectionButton;

    public WebElement getWelcomeLeftSideMenuLink() {
        return welcomeLeftSideMenuLink;
    }

    public WebElement getOurPassionLeftSideMenuLink() {
        return ourPassionLeftSideMenuLink;
    }

    public WebElement getMenuLeftSideMenuLink() {
        return menuLeftSideMenuLink;
    }

    public WebElement getLetsTalkLeftSideMenuLink() {
        return letsTalkLeftSideMenuLink;
    }

    public WebElement getCheckOutLeftSideMenuLink() {
        return checkOutLeftSideMenuLink;
    }

    public WebElement getOrganicTeaLinkSideBar() {
        return organicTeaLinkSideBar;
    }

    public WebElement getTeaOfMonthClubLinkSideBar() {
        return teaOfMonthClubLinkSideBar;
    }

    public WebElement getHerbalTeaCollectionButton() {
        return herbalTeaCollectionButton;
    }

    public WebElement getLooseTeaCollectionButton() {
        return looseTeaCollectionButton;
    }

    public WebElement getFalvouredTeaCollectionButton() {
        return falvouredTeaCollectionButton;
    }


}
