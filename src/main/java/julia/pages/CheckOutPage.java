package julia.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckOutPage {
    public WebDriver driver;

    public CheckOutPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // css selector by id
    @FindBy(css = "[id*='451989411']")
    private WebElement pageTitle;

    @FindBy(css = "#email")
    private WebElement emailInputField;

    @FindBy(css = "#name")
    private WebElement nameInputField;

    @FindBy(css = "#address")
    private WebElement addressInputField;

    @FindBy(css = "#card_type")
    private WebElement cardTypeDropDownMenu;

    @FindBy(css = "#card_number")
    private WebElement cardNumberInputField;

    @FindBy(css = "#cardholder_name")
    private WebElement cardHolderNameInputField;

    @FindBy(css = "#verification_code")
    private WebElement verififcationCodeInputField;

    //css example class, a is a tag name OR can be .btn
    @FindBy(css = "a.btn")
    private WebElement cancelButton;

    //css example class with spaces, a is a tag name
    @FindBy(css = "button.btn.btn-primary")
    private WebElement placeOrderButton;

    public WebElement getEmailInputField() {
        return emailInputField;
    }

    public WebElement getNameInputField() {
        return nameInputField;
    }

    public WebElement getAddressInputField() {
        return addressInputField;
    }

    public WebElement getCardTypeDropDownMenu() {
        return cardTypeDropDownMenu;
    }

    public WebElement getCardNumberInputField() {
        return cardNumberInputField;
    }

    public WebElement getCardHolderNameInputField() {
        return cardHolderNameInputField;
    }

    public WebElement getVerififcationCodeInputField() {
        return verififcationCodeInputField;
    }

    public WebElement getCancelButton() {
        return cancelButton;
    }

    public WebElement getPlaceOrderButton() {
        return placeOrderButton;
    }

    public WebElement getPageTitle() {
        return pageTitle;
    }
}
