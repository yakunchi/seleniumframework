package julia.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LetsTalkTeaPage {

    public WebDriver driver;

    public LetsTalkTeaPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "[id*='914913']")
    private WebElement pageTitle;

    @FindBy(css = "input[name='name']")
    private WebElement nameInputField;

    @FindBy(css = "input[name='email']")
    private WebElement emailInputField;

    @FindBy(css = "input[name='subject']")
    private WebElement subjectInputField;

    @FindBy(css = "textarea[name='message']")
    private WebElement messageInputField;

    //css example selection by 2 attributes
    @FindBy(css = "input[type='submit'][value='Submit']")
    private WebElement submitButton;

    public WebElement getNameInputField() {
        return nameInputField;
    }

    public WebElement getEmailInputField() {
        return emailInputField;
    }

    public WebElement getSubjectInputField() {
        return subjectInputField;
    }

    public WebElement getMessageInputField() {
        return messageInputField;
    }

    public WebElement getSubmitButton() {
        return submitButton;
    }

    public WebElement getPageTitle() {
        return pageTitle;
    }
}
