package julia.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MenuPage {
    public WebDriver driver;

    public MenuPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //css example class name
    @FindBy(css = ".txt")
    private WebElement pageTitle;

    //contains example
    @FindBy(css = "[id*='1959280'][class='wsb-element-button']")
    private WebElement redTeaCheckout;

    @FindBy(css = "[id*='1955160'][class='wsb-element-button']")
    private WebElement greenTeaCheckout;

    @FindBy(css = "[id*='1961556'][class='wsb-element-button']")
    private WebElement oolongTeaCheckout;

    public WebElement getPageTitle() {
        return pageTitle;
    }

    public WebElement getRedTeaCheckout() {
        return redTeaCheckout;
    }

    public WebElement getGreenTeaCheckout() {
        return greenTeaCheckout;
    }

    public WebElement getOolongTeaCheckout() {
        return oolongTeaCheckout;
    }


    //span.button-content.wsb-button-content or [id*="1959280" or with class [id*="1959280"][class="wsb-element-button"]
}
