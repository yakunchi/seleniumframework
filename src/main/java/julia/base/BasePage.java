package julia.base;

import org.apache.maven.shared.utils.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BasePage {
    public WebDriver driver;
    public Properties property;

    public WebDriver initializeDriver() throws IOException {
        property = new Properties();
        FileInputStream propertyFile = new FileInputStream("src/main/resources/data.properties");
        property.load(propertyFile);
        String browser = property.getProperty("browser");

        if (browser.equals("chrome")) {
            String webDriverRelativePath = "src/main/resources/chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", webDriverRelativePath);
            driver = new ChromeDriver();
        } else {
            String webDriverRelativePath = "src/main/resources/geckodriver.exe";
            System.setProperty("webdriver.gecko.driver", webDriverRelativePath);
            driver = new FirefoxDriver();
        }
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void getScreenshot(String screenShotName) throws IOException {
        File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String filePath = String.format("src/main/resources/%s.png", screenShotName);
        FileUtils.copyFile(src, new File(filePath));
    }

    public void clickOnElement(WebElement webElement) {
        webElement.click();
    }

    public void sendKeysToElement(WebElement webElement, String text) {
        webElement.sendKeys(text);
    }

    public String getText(WebElement webElement) {
        return webElement.getText();

    }
}
