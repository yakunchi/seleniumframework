package julia.utils;

import julia.base.BasePage;
import org.apache.maven.shared.utils.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.io.IOException;

public class Listeners extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult result) {

        Object currentClass = result.getInstance();
        WebDriver driver = ((BasePage) currentClass).getDriver();

        String screenShotName = result.getName();

        File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String filePath = String.format("src/main/resources/test-output/%s.png", screenShotName);
        try {
            FileUtils.copyFile(src, new File(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
