package julia.pages;

import julia.base.BasePage;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import java.io.IOException;

import static org.testng.Assert.*;

@Listeners({julia.utils.Listeners.class})
public class MenuPageTest extends BasePage {
    HomePage homePage;
    MenuPage menuPage;
    CheckOutPage checkOutPage;

    @BeforeTest
    public void initialize() throws IOException {
        driver = initializeDriver();
        homePage = new HomePage(driver);
        menuPage = new MenuPage(driver);
        checkOutPage = new CheckOutPage(driver);
    }

    @BeforeMethod
    public void setUp() {
        driver.get(property.getProperty("home"));
        clickOnElement(homePage.getMenuLeftSideMenuLink());
    }

    @Test(dataProvider = "getData")
    public void verifyCheckOutButton(WebElement teaCheckoutButton) {
        clickOnElement(teaCheckoutButton);
        String pageTitle = checkOutPage.getPageTitle().getText();
        assertEquals(pageTitle, "Pay with Credit Card or Log In");
    }

    @DataProvider
    public Object[][] getData() {
        Object[][] webElements = new Object[3][1];
        webElements[0][0] = menuPage.getGreenTeaCheckout();
        webElements[1][0] = menuPage.getOolongTeaCheckout();
        webElements[2][0] = menuPage.getRedTeaCheckout();
        return webElements;
    }

    @AfterTest
    public void tearDown() {
        driver.close();
    }


}

