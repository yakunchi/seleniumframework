package julia.pages;

import julia.base.BasePage;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.*;

import java.io.IOException;

import static org.testng.Assert.*;

public class CheckOutPageTest extends BasePage {
    HomePage homePage;
    CheckOutPage checkOutPage;
    MenuPage menuPage;
    Select select;

    @BeforeTest
    public void initialize() throws IOException {
        driver = initializeDriver();
        homePage = new HomePage(driver);
        checkOutPage = new CheckOutPage(driver);
        menuPage = new MenuPage(driver);

    }

    @BeforeMethod
    public void setUp() {
        driver.get(property.getProperty("home"));
        clickOnElement(homePage.getCheckOutLeftSideMenuLink());
        sendKeysToElement(checkOutPage.getEmailInputField(), "email@gmail.com");
        sendKeysToElement(checkOutPage.getNameInputField(), "Jon Doe");
        sendKeysToElement(checkOutPage.getAddressInputField(), "111 8th Ave, New York, NY 10011, USA");
        sendKeysToElement(checkOutPage.getCardNumberInputField(), "1111 2222 3333 4444");
        sendKeysToElement(checkOutPage.getCardHolderNameInputField(), "Jon Doe");
        sendKeysToElement(checkOutPage.getVerififcationCodeInputField(), "111");
        select = new Select(checkOutPage.getCardTypeDropDownMenu());
    }

    @Test
    public void visaCardTest() {
        select.selectByVisibleText("Visa");
        clickOnElement(checkOutPage.getPlaceOrderButton());
        String pageTitle = menuPage.getPageTitle().getText();
        assertEquals(pageTitle, "Menu");
    }

    @Test
    public void masterCardTest() {
        select.selectByVisibleText("Mastercard");
        clickOnElement(checkOutPage.getPlaceOrderButton());
        String pageTitle = menuPage.getPageTitle().getText();
        assertEquals(pageTitle, "Menu");
    }

    @Test
    public void americanExpressCardTest() {
        select.selectByVisibleText("American Express");
        clickOnElement(checkOutPage.getPlaceOrderButton());
        String pageTitle = menuPage.getPageTitle().getText();
        assertEquals(pageTitle, "Menu");
    }

    @Test
    public void dinersClubCardTest() {
        select.selectByVisibleText("Diners Club");
        clickOnElement(checkOutPage.getPlaceOrderButton());
        String pageTitle = menuPage.getPageTitle().getText();
        assertEquals(pageTitle, "Menu");
    }

    @Test
    public void noCardSelectedTest() {
        select.selectByVisibleText("");
        clickOnElement(checkOutPage.getPlaceOrderButton());
        String pageTitle = menuPage.getPageTitle().getText();
        assertEquals(pageTitle, "Menu");
    }

    @Test
    public void cancelOrderTest() {
        select.selectByVisibleText("");
        clickOnElement(checkOutPage.getCancelButton());
        String pageTitle = menuPage.getPageTitle().getText();
        assertEquals(pageTitle, "Menu");
    }

    @AfterTest
    public void tearDown() {
        driver.close();
    }

}