package julia.pages;

import julia.base.BasePage;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

public class HomePageTest extends BasePage {
    HomePage homePage;
    MenuPage menuPage;

    @BeforeTest
    public void initialize() throws IOException {
        driver = initializeDriver();
        homePage = new HomePage(driver);
        menuPage = new MenuPage(driver);
    }

    @BeforeMethod
    public void setUp() {
        driver.get(property.getProperty("home"));
    }

    @Test(dataProvider = "getData")
    public void herbalTea(WebElement teaCollection) {
        clickOnElement(teaCollection);
        String pageTitle = menuPage.getPageTitle().getText();
        assertEquals(pageTitle, "Menu");
    }

    //data provider example
    @DataProvider
    public Object[][] getData() {
        Object[][] webElements = new Object[5][1];
        webElements[0][0] = homePage.getHerbalTeaCollectionButton();
        webElements[1][0] = homePage.getFalvouredTeaCollectionButton();
        webElements[2][0] = homePage.getLooseTeaCollectionButton();
        webElements[3][0] = homePage.getTeaOfMonthClubLinkSideBar();
        webElements[4][0] = homePage.getOrganicTeaLinkSideBar();
        return webElements;

    }

    @AfterTest
    public void tearDown() {
        driver.close();
    }

}