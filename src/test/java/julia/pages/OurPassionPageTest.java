package julia.pages;

import julia.base.BasePage;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.IOException;

import static org.testng.Assert.*;

public class OurPassionPageTest extends BasePage {
    OurPassionPage ourPassionPage;
    SoftAssert softAssertion;

    @BeforeTest
    public void initialize() throws IOException {
        driver = initializeDriver();
        ourPassionPage = new OurPassionPage(driver);
        softAssertion = new SoftAssert();
    }

    @BeforeMethod
    public void setUp() {
        driver.get(property.getProperty("ourPassionPage"));
    }

    @Test
    public void verifyPageContent() {
        String ourPassionTitleText = getText(ourPassionPage.getOurPassionTitle());
        String ourExpertTitleText = getText(ourPassionPage.getTheExpertsTitle());
        softAssertion.assertEquals(ourPassionTitleText, "Our Passion");
        softAssertion.assertEquals(ourExpertTitleText, "The Experts");
        softAssertion.assertAll();
    }

    @AfterTest
    public void tearDown() {
        driver.close();
    }

}