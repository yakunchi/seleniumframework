package julia.pages;

import julia.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.*;

import java.io.IOException;

import static org.testng.Assert.*;

public class LetsTalkTeaPageTest extends BasePage {
    HomePage homePage;
    LetsTalkTeaPage letsTalkTeaPage;

    @BeforeTest
    public void initialize() throws IOException {
        driver = initializeDriver();
        homePage = new HomePage(driver);
        letsTalkTeaPage = new LetsTalkTeaPage(driver);
    }

    @BeforeMethod
    public void setUp() {
        driver.get(property.getProperty("home"));
        clickOnElement(homePage.getLetsTalkLeftSideMenuLink());
    }

    @Test
    public void sendLetterToCompany() {

        sendKeysToElement(letsTalkTeaPage.getEmailInputField(), "email@gmail.com");
        sendKeysToElement(letsTalkTeaPage.getNameInputField(), "Jon Doe");
        sendKeysToElement(letsTalkTeaPage.getSubjectInputField(), "Great tea.");
        sendKeysToElement(letsTalkTeaPage.getMessageInputField(), "We like your tea.");
        clickOnElement(letsTalkTeaPage.getSubmitButton());

        String pageTitle = letsTalkTeaPage.getPageTitle().getText();
        assertEquals(pageTitle, "Let's Talk Tea");
    }


    @AfterTest
    public void tearDown() {
        driver.close();
    }


}