# Selenium Page Object Model

##What was used:

*	Java
*	Selenium
*	TestNG
*	Extent reports
*	Jenkins

##What was implemented:
*	Non static WebDriver.
*	Main variables, as browser and links are stored in file properties.
*	Listeners - screen shot on the test failure.
*	Selectors - XPath and CSS.
*	Soft assertions.
*	Data provider.
